<?php
/**
 * Plugin Name: phpMyAdmin
 * Plugin URI: https://serverpress.com/plugins/phpmyadmin
 * Description: phpMyAdmin provided as a pre-configured DesktopServer plugin.
 * Version: 5.1.3
 * Author: Stephen J. Carnam
 * Author URI: http://serverpress.com
 * Tags: must-use
 */
